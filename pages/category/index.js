// pages/category/goodsList.js
var app = getApp();
const config = require('../../config');
const host = config.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categoryList:[],// 分类列表
    secondList: [], // 二级分类列表
    chooseTab:0, // 当前选择的1级分类
    windowHeight:null, // 屏幕高度
  },
  /**
	 * 获取分类列表
	 */
  getCateGory: function (cb) {
    var that = this
    wx.request({
      url: host + '/wxapp/cate/getCategoryList',
      method: "GET",
      header: { 'content-type': 'application/json' },
      success: function (res) {
        if (res.data.ok == true) {
          console.log(res);
          that.setData({
            categoryList: res.data.content.categoryList
          })
          typeof cb == "function" && cb(res.data.content.categoryList)
        }
      }
    })
  },
  /**
     * 点选一级分类操作
     */
  chooseFirstCate: function (e) {
  
    var that = this
    var index = e.currentTarget.dataset.index
    that.setData({
      chooseTab: index
    })
    var categoryList = that.data.categoryList
    var secondList = categoryList[index]['goodsList']
    if (secondList) {
      that.setData({
        secondList: secondList,
      })
    } else {
      that.setData({
        secondList: null
      })
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    app.setPageTitle("分类列表")
    wx.getSystemInfo({
      success:function(res){
        that.setData({
          windowHeight:res.windowHeight
        })
      }
    })
    that.getCateGory(function (categoryList) {
      that.setData({
        secondList: categoryList[0].sonList
      })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this
    that.getCateGory(function (categoryList) {
      that.setData({
        secondList: categoryList[0].sonList
      })
    })
    wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})