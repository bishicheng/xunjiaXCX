// pages/category/index.js
var app = getApp();
const config = require('../../config');
const host = config.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cateName:null,
    cateID:null,
		goodsList:[],
		buyGoodsID: null,// 出价商品id
		buyGoodsPrice: null,// 价格
		buyBoxStatus: false,// 是否显示出价输入框
		webUserInfo: null, // 用户站点信息
    page: 1,
    mobileInputStatus: true, // 绑定手机框
    newMobile: null,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    that.setData({
      cateName:options.cateName,
      cateID:options.cateID,
    })
    app.setPageTitle(options.cateName)
    that.getGoodsList(options.cateID)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
		var that = this
		if (app.globalData.webUserInfo != null) {
			that.setData({
				webUserInfo: app.globalData.webUserInfo
			})
		} else {
			//调用应用实例的方法获取全局数据
			app.getUserInfo(function (userInfo) {
				//更新数据 open
				that.setData({
					userInfo: userInfo
				})
				// 获取用openid
				app.getOpenID(function (openID) {
					var nickName = userInfo.nickName
					var headPic = userInfo.avatarUrl
					// 获取用户在站点的信息
					app.getWebUserInfo(openID, nickName, headPic, function (webUserInfo) {
						if (webUserInfo) {
							that.setData({
								webUserInfo: webUserInfo
							})
						}
					})
				})
			})
		}
  },
  /**
   * 获取商品列表
   */
	getGoodsList:function(cateID){
		var that = this
		wx.request({
			url: host + '/wxapp/goods/getGoodsList',
			data: {
				cateID: cateID,
        page: that.data.page,
			},
			method: "GET",
			header: { 'content-type': 'application/json' },
			success: function (res) {
        console.log(res);
				if (res.data.ok == true) {
          var list = that.data.goodsList;
          for (var i = 0; i < res.data.content.goodsList.content.length; i++) {
            list.push(res.data.content.goodsList.content[i]);
          }
          that.setData({
            goodsList: list
          })
				}else{
          wx.showToast({
            title: '没有更多了',
            image: "../../images/error2.png"
          })
        }
			}
		})
	},
	/**
   * 点击出价
   */
  doBuy: function (e) {
    var that = this
    var goodsID = e.detail.value.goodsID
    that.setData({
      buyGoodsPrice: null,
    })

    if (app.globalData.webUserInfo.deposit) {
      // 如果用户信息存在，收集formid
      var formID = e.detail.formId;
      if (formID != "the formId is a mock one") {
        app.collectFormID(formID)
      }

      if (app.globalData.webUserInfo.mobile) {
        that.setData({
          buyBoxStatus: true,
          buyGoodsID: goodsID,
        })
      } else {
        that.setData({
          newMobile: null,
          mobileInputStatus: !that.data.mobileInputStatus,
        })
      }
    } else {
      wx.showModal({
        title: '提示',
        content: '请先交保证金！',
        success: function (res) {
          if (res.confirm) {
            app.payDeposit()
          }
        }
      })
    }
  },
  /**
   * 确认出价点击发送
   */
	sendPrice: function () {
		var that = this
		var goodsPrice = that.data.buyGoodsPrice
		if (that.data.webUserInfo) {
			var uid = that.data.webUserInfo.id
			var goodsID = that.data.buyGoodsID
			app.doSendPrice(uid, goodsPrice, goodsID)
		}
	},
  /**
   * 出价失去焦点
   */
	buyBlur: function () {
		var that = this
		that.setData({
			buyBoxStatus: 0
		})
	},
  /**
   * 变更价格
   */
	changeBuyGoodsPrice: function (e) {
		this.setData({
			buyGoodsPrice: e.detail.value
		})
	},
  /**
   * 变更联系电话
   */
  changeMobileInput: function (e) {
    var that = this
    var mobile = e.detail.value
    that.setData({
      newMobile: mobile
    })
  },
  /**
   * 确认修改手机号
   */
  changeMobileConfirm: function () {
    var that = this
    if (that.data.newMobile) {
      var pattern = /^1[3|4|5|8|9]\d{9}$/
      var flag = pattern.test(that.data.newMobile);
      if (!flag) {
        wx.showToast({
          title: '格式不正确！',
          image: "../../images/error2.png",
          duration: 2000
        })
        return
      }
    } else {
      wx.showToast({
        title: '请输入手机号！',
        image: "../../images/error2.png"
      })
      return
    }
    wx.request({
      url: host + '/wxapp/index/changeMobile',
      data: {
        mobile: that.data.newMobile,
        openID: app.globalData.userInfo.openID
      },
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function (res) {
        if (res.data.ok == true) {
          that.setData({
            webUserInfo: res.data.content.userInfo,
            mobileInputStatus: !that.data.mobileInputStatus,
          })
          app.globalData.webUserInfo = res.data.content.userInfo
          wx.showToast({
            title: '修改成功',
          })
        } else {
          wx.showToast({
            title: '修改失败！',
            image: "../../images/error2.png"
          })
        }
      }
    })
  },
  /**
   * 取消修改
   */
  changeMobileCancel: function () {
    var that = this
    that.setData({
      newMobile: null,
      mobileInputStatus: !that.data.mobileInputStatus,
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    app.sendFormIDs()
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    app.sendFormIDs()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this
    var cateID = that.data.cateID
    if(cateID){
      that.setData({
        goodsList:[],
        page: 1
      })
      that.getGoodsList(cateID)
    }
		wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var that = this
    that.setData({
      page: that.data.page + 1
    })
    that.getGoodsList(that.data.cateID,that.data.page)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})