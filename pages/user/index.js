// pages/user/index.js
var app = getApp();
const config = require('../../config');
const host = config.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    webUserInfo: null,
    mobileInputStatus: true,
    newMobile:null,
    containerStatus:null,
    newUsername: null,
    newAddress: null,
    newLinkman: null
  },
  /**
   * 变更联系电话
   */
  changeMobileInput:function(e){
    var that = this
    var mobile = e.detail.value
    that.setData({
      newMobile:mobile
    })
  },
  /**
   * 变更联系人电话
   */
  changeLinkmanInput: function (e) {
    var that = this
    var linkman = e.detail.value
    that.setData({
      newLinkman: linkman
    })
  },
  /**
   * 变更名称
   */
  changeUsernameInput: function (e) {
    var that = this
    var username = e.detail.value
    that.setData({
      newUsername: username
    })
  },
  /**
   * 变更联系电话
   */
  changeAddressInput: function (e) {
    var that = this
    var address = e.detail.value
    that.setData({
      newAddress: address
    })
  },
  /**
   * 确认修改手机号
   */
  changeMobileConfirm:function(){
    var that = this
    if (that.data.newMobile && that.data.newUsername && that.data.newLinkman && that.data.newAddress) {
      var pattern = /^1[3|4|5|8|9]\d{9}$/
      var flag = pattern.test(that.data.newMobile);
      if (!flag) {
        wx.showToast({
          title: '手机号格式错误！',
          image: "../../images/error2.png",
          duration: 2000
        })
        return
      }
    }else{
      wx.showToast({
        title: '请补齐信息！',
        image: "../../images/error2.png"
      })
      return
    }
    wx.request({
      url: host+'/wxapp/index/changeMobile',
      data:{
        mobile:that.data.newMobile,
        username: that.data.newUsername,
        linkman: that.data.newLinkman,
        address: that.data.newAddress,
        openID:app.globalData.userInfo.openID
      },
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success:function(res){
        if(res.data.ok == true){
          that.setData({
            webUserInfo:res.data.content.userInfo,
            mobileInputStatus: !that.data.mobileInputStatus,
          })
          app.globalData.webUserInfo = res.data.content.userInfo
          wx.showToast({
            title: '修改成功',
          })
        }else{
          wx.showToast({
            title: '修改失败！',
            image:"../../images/error2.png"
          })
        }
      }
    })
  },
  /**
   * 取消修改
   */
  changeMobileCancel:function(){
    var that = this
    that.setData({
      newMobile:null,
      mobileInputStatus: !that.data.mobileInputStatus,
    })
  },
  /**
   * 显示修改手机输入框
   */
  showMobileInputBox:function(e){
    var that = this
    // 收集formid
    var formID = e.detail.formId;
    if (formID != "the formId is a mock one") {
      app.collectFormID(formID)
    }
    that.setData({
      newMobile: null,
      mobileInputStatus: !that.data.mobileInputStatus,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
		app.setPageTitle("个人中心")
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    
    if (app.globalData.webUserInfo == null) {
      //如果不存在用户信息
      if (app.globalData.userInfo == null) {
        wx.openSetting({
          success: function () {
            wx.getUserInfo({
              success: function (res) {
                app.globalData.userInfo = res.userInfo
               
              }
            })
          },
        })
      } else {
        app.getWebUserInfo(app.globalData.userInfo.openID, app.globalData.userInfo.nickName, app.globalData.userInfo.avatarUrl, function (webUserInfo) {
          console.log(1);
          if (webUserInfo) {
            that.setData({
              userInfo: app.globalData.userInfo,
              webUserInfo: webUserInfo,
              containerStatus: true,
            })
          }
        })
      }
    } else {
      // 每次显示页面都去更新用户最新信息
      app.getWebUserInfo(app.globalData.userInfo.openID, app.globalData.userInfo.nickName, app.globalData.userInfo.avatarUrl, function (webUserInfo) {
        if (webUserInfo) {
          that.setData({
            webUserInfo: webUserInfo,
            containerStatus: true,
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    app.sendFormIDs()
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    app.sendFormIDs()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this
    if (app.globalData.webUserInfo == null) {
      //如果不存在用户信息
      if (app.globalData.userInfo == null) {
        wx.openSetting({
          success: function () {
            wx.getUserInfo({
              success: function (res) {
                app.globalData.userInfo = res.userInfo
              }
            })
          },
        })
      } else {
        that.setData({
          userInfo: app.globalData.userInfo
        })
      }
    } else {
      // 每次显示页面都去更新用户最新信息
      app.getWebUserInfo(app.globalData.userInfo.openID, app.globalData.userInfo.nickName, app.globalData.userInfo.avatarUrl, function (webUserInfo) {
        if (webUserInfo) {
          that.setData({
            webUserInfo: webUserInfo
          })
        }
      })
    }
		wx.stopPullDownRefresh()

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})