// pages/user/order.js
var app = getApp();
const config = require('../../config');
const host = config.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderList:[],
		page: 1,
		buyGoodsID: null,// 出价商品id
		buyGoodsPrice: null,// 价格
		buyBoxStatus: false,// 是否显示出价输入框
		webUserInfo: null, // 用户站点信息
    loadTime:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
		app.setPageTitle("我的")
  },
	/**
   * 点击出价
   */
  doBuy: function (e) {
    var that = this
    var goodsID = e.detail.value.goodsID
    that.setData({
      buyGoodsPrice: null,
    })

    if (app.globalData.webUserInfo.deposit) {
      // 如果用户信息存在，收集formid
      var formID = e.detail.formId;
      if (formID != "the formId is a mock one") {
        app.collectFormID(formID)
      }

      if (app.globalData.webUserInfo.mobile) {
        that.setData({
          buyBoxStatus: true,
          buyGoodsID: goodsID,
        })
      } else {
        that.setData({
          newMobile: null,
          mobileInputStatus: !that.data.mobileInputStatus,
        })
      }
    } else {
      wx.showModal({
        title: '提示',
        content: '请先交保证金！',
        success: function (res) {
          if (res.confirm) {
            app.payDeposit()
          }
        }
      })
    }
  },
  /**
   * 确认出价点击发送
   */
	sendPrice: function () {
		var that = this
		var goodsPrice = that.data.buyGoodsPrice
		if (that.data.webUserInfo) {
			var uid = that.data.webUserInfo.id
			var goodsID = that.data.buyGoodsID
			app.doSendPrice(uid, goodsPrice, goodsID,function(){
        that.getOrderList()
      })
		}
	},
  /**
   * 出价失去焦点
   */
	buyBlur: function () {
		var that = this
		that.setData({
			buyBoxStatus: 0
		})
	},
  /**
   * 变更价格
   */
	changeBuyGoodsPrice: function (e) {
		this.setData({
			buyGoodsPrice: e.detail.value
		})
	},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    if (app.globalData.webUserInfo == null) {
      //如果不存在用户信息
      if (app.globalData.userInfo == null) {
        wx.openSetting({
          success: function () {
            wx.getUserInfo({
              success: function (res) {
                app.globalData.userInfo = res.userInfo
              }
            })
          },
        })
      } else {
        that.setData({
          userInfo: app.globalData.userInfo
        })
      }
    } else {
      that.setData({
        webUserInfo: app.globalData.webUserInfo,
      })
    }
		that.getOrderList()
  },
  /**
   * 获取我的所有叫价单
   */
  getOrderList:function(){
    var that = this
    wx.request({
			url: host +'/wxapp/order/getOrderList',
      data: {
        uid: app.globalData.webUserInfo.id
      },
      method: "POST",
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success:function(res){
        if(res.data.ok == true){
          that.setData({
            loadTime:that.data.loadTime+1,
            orderList:res.data.content.orderList
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    app.sendFormIDs()

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    app.sendFormIDs()

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getOrderList()
    wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})