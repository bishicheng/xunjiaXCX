// pages/user/deposit.js
var app = getApp()
const config = require('../../config');
const host = config.host;
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
		app.setPageTitle("竞标须知")
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
		wx.stopPullDownRefresh()
		
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  /**
   * 发起支付保证金
   */
  payDeposit: function () {
    var openID = app.globalData.userInfo.openID
    if (openID) {
      // 发起支付请求
      wx.request({
        url: host + '/wxapp/index/order',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        data: {
          openID: openID
        },
        method: 'POST',
        success: function (response) {
          wx.requestPayment({
            timeStamp: response.data.timeStamp,
            nonceStr: response.data.nonceStr,
            package: response.data.package,
            signType: response.data.signType,
            paySign: response.data.paySign,
            success:function(res){
              // 付款成功 更新用户信息
              wx.request({
                url: host+'/wxapp/index/getUserInfo',
                data:{openID:app.globalData.userInfo.openID},
                method: "POST",
                header: {
                  'content-type': 'application/x-www-form-urlencoded'
                },
                success:function(r){
                  console.log(r.data)
                  app.globalData.webUserInfo = r.data.userInfo
                  wx.switchTab({
                    url: '../index/index',
                  })
                }
              })
            },
            fail:function(res){
              console.log(res)
            }
          })
        }
      })
    }
    
  }
})